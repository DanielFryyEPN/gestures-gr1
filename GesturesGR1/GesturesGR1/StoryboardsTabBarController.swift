//
//  StoryboardsTabBarController.swift
//  GesturesGR1
//
//  Created by Daniel Freire on 23/1/18.
//  Copyright © 2018 Daniel Freire. All rights reserved.
//

import UIKit

class StoryboardsTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let left = tabBar.items![0]
        let right = tabBar.items![1]
        
        left.image = #imageLiteral(resourceName: "ic_copyright")
        right.image = #imageLiteral(resourceName: "ic_backup")
        
        left.title = "Tap"
        right.title = "Swipe"
        
        left.badgeColor = .red
        left.badgeValue = "6"
    }
}
