//
//  TapGestureViewController.swift
//  GesturesGR1
//
//  Created by Daniel Freire on 17/1/18.
//  Copyright © 2018 Daniel Freire. All rights reserved.
//

import UIKit

class TapGestureViewController: UIViewController {
    
    // MARK :- Outlets
    @IBOutlet weak var touchesLabel: UILabel!
    @IBOutlet weak var tapsLabel: UILabel!
    @IBOutlet weak var coordLabel: UILabel!
    @IBOutlet weak var customView: UIView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touchCount = touches.count
        let tap = touches.first
        let tapCount = tap?.tapCount
        
        touchesLabel.text = "\(touchCount)"
        tapsLabel.text = "\(tapCount ?? 0)"
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        let point = touch?.location(in: self.view)
        
        let x = point?.x
        let y = point?.y
        
        coordLabel.text = "x: \(x!), y: \(y!)"
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Ended!")
    }
    
    // MARK :- Actions
    @IBAction func tapGestureAction(_ sender: UITapGestureRecognizer) {
        
        customView.backgroundColor = .red
    }
    
}
