//
//  SwipeGestureViewController.swift
//  GesturesGR1
//
//  Created by Daniel Freire on 17/1/18.
//  Copyright © 2018 Daniel Freire. All rights reserved.
//

import UIKit

class SwipeGestureViewController: UIViewController {

    @IBOutlet weak var customView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func swipeUpAction(_ sender: Any) {
        
        customView.backgroundColor = .black
    }
    
    @IBAction func swipeDownAction(_ sender: Any) {
        customView.backgroundColor = .blue
    }
    
    @IBAction func swipeLeftAction(_ sender: Any) {
        customView.backgroundColor = .red
    }
    
    @IBAction func swipeRightAction(_ sender: Any) {
        customView.backgroundColor = .green
    }
}
